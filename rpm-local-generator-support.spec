Summary: RPM local_generator support
Name: rpm-local-generator-support
Version: 1
Release:1%{?dist}

License: MIT
URL: https://src.fedoraproject.org/rpms/rpm-local-generator-support
BuildArch: noarch

%description
local_generator.attr file enabling RPM dependency generator to be used on .spec
files, which ships them.


%prep


%build


%install
install -d %{buildroot}%{_fileattrsdir}
touch %{buildroot}%{_fileattrsdir}/local_generator.attr


%files
%{_fileattrsdir}/local_generator.attr



%changelog
* Fri Oct 06 2023 Vít Ondruch <vondruch@redhat.com> - 1-1
- Initial version.
